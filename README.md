### Data Engineering Specialty
> Designed and authored by Prime Data Engineering Team

This training is designed around the day-to-day duties of a Data Engineer at Prime, and it consists of a variety of domains that enable data-driven solutions that Prime provides to its clients. Hence, the content of the training will prepare the participants with required know-how for real-world projects, from both conceptual and technology standpoints.  In addition, this training will cover all of the major areas that you'll need to know to be able to design, build, deploy, and monitor data processing applications, by leveraging cutting-edge BigData technologies in a cloud-based environment.


**A person who completes the training successfully will become intimately familiar with the following topics:**
* Wear our tool-belt (Agile, Linux, Git, Git tools, Atlassian)
* Database Design and Normalisation
* SQL vs NoSQL database systems
* Ecosystem of Big Data
* Working with Apache Hadoop and Hive
* Developing ETL pipelines with Apache Spark (batch and streaming analysis)
* AWS essentials
* Working with AWS BigData services, including S3, EMR, Glue, Kinesis, and Athena
* Columnar Databases and AWS Redshift
* Architecting AWS-based serverless data lakes
