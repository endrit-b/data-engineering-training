## Agile Methodology

AGILE methodology is a practice that promotes continuous iteration of development and testing throughout the software development life cycle of the project. Both development and testing activities are concurrent unlike the Waterfall model.

The agile software development emphasizes on four core values:
1. Individual and team interactions over processes and tools
2. Working software over comprehensive documentation
3. Customer collaboration over contract negotiation
4. Responding to change over following a plan

    > ![Fork repo](./_img/agile_methodology.jpg)

Here are some key points on how an agile model works:
1. Agile method proposes incremental and iterative approach to software design
2. The agile process is broken into individual models that designers work on
3. The customer has early and frequent opportunities to look at the product and make decisions and changes to the project
4. Agile model is considered unstructured compared to the waterfall model
5. Small projects can be implemented very quickly. For large projects, it is difficult to estimate the development time.
6. Error can be fixed in the middle of the project.
7. Development process is iterative, and the project is executed in short (2-4) weeks iterations. Planning is very less.
8. Documentation attends less priority than software development
9. Every iteration has its own testing phase. It allows implementing regression testing every time new functions or logic are released.
10. In agile testing when an iteration end, shippable features of the product is delivered to the customer. New features are usable right after shipment. It is useful when you have good contact with customers.
11. Testers and developers work together
12. At the end of every sprint, user acceptance is performed
13. It requires close communication with developers and together analyze requirements and planning


#### Scrum

SCRUM is an agile development method which concentrates specifically on how to manage tasks within a team-based development environment. 

Scrum believes in empowering the development team and advocates working in small teams (say- 7 to 9 members). It consists of three roles, and their responsibilities are explained as follows:

Here are some key points on how an agile model works:
1. Scrum Master - Master is responsible for setting up the team, sprint meeting and removes obstacles to progress. Preferably, a highly skilled technical guy!
2. Product owner - The Product Owner creates product backlog, prioritizes the backlog and is responsible for the delivery of the functionality at each iteration. Product owner aligns with both the development team but also business owners to make sure that the development is going according to plan and client requirements
3. Scrum Team - Team manages its own work and organizes the work to complete the sprint or cycle

The project development process is organised into sprints. Sprints last 1-2 weeks depending on the nature of the project. Before a sprint starts, a sprint planning meeting is held in order to pick out the tasks that will be done at the next sprint.

**Every Sprint will have the following events:**
1. Sprint planning. The purpose is to identify, estimate and assign all the tasks that the team is ready to commit to doing, considering the sprint time. These are the questions answered during a sprint planning:
    * What are the most pressing issues and how much effort will it take the Scrum Team to do. Ways to answer: Open Discussions, Poker planning, Brainstorms
    * What can we do in 2 weeks?
    * Who should do what?
    * What are the dependencies between these tasks?
    * Do we need to perform any research?
    
2. Daily Stand-Up. It is a short meeting, max 5 min per participant, where everyone answers the following questions:
    * What did I do yesterday?
    * What am I going to do today?
    * Do I have any impediments?
    
3. Sprint Review. Its purpose is to have an alignment with the stakeholders. See how the sprint went, and ideally, see what the next changes are going to be published. The following questions will be targeted:
    * How did the sprint go?
    * Did we manage as a team to complete all the tasks?
    * How do the new features look like (Demo)?

4. Sprint Retrospective. The development team at this meeting, comes together to discuss how the sprint went internally. The following questions will be answered:
    * What went well?
    * What went wrong?
    * How can we improve?
    
    > ![Fork repo](./_img/scrum.png)


The general idea behind SCRUM, is that at the end of every Sprint a new product deliverable is created. That means the features under development were developed, tested and merged to the repository and are ready to be published at the end of the sprint. In general, the ideal use case for a developer is to:

* Always release fast and often. This way, there won’t be too many code changes to be managed (it is harder to publish something, that you’ve been working on for 2 years vs 2 weeks).
* The code released would get immediate feedback, which will be included in the backlog (to be improved at the sprints in the future)

For the SCRUM method to work is it crucial to maintain the backlog such that it is organised, and the tasks contain a clear description and a definition of done.

There are different approaches on how to estimate tickets. You can use the hour estimation and/or other means of estimations. 
PRIME's preferred way of estimating is using fibonacci point system. It looks like this:
* 1 Point -> a few hours (1-3 hours)
* 2 Points -> 3-5 hours
* 3 Points -> Whole working day
* 5 Points -> 2-3 days
* 8 Points -> Whole week
* 13 Points - Whole Sprint

Other ways of handling estimations include complexity vs hours. That can be explained like this
Hour Estimation:
* Agon: “I can run to school from PRIME for 8 hours”
* Leutrim: “I can run to school from PRIME for 6 hours”


**Complexity estimation:**

Agon and Leutrim both agree on the fact that “School is 5km away!”. So rather then estimating individually on the task and how much it would take you personally to accomplish it, you would rather focus on the complexity of the task (fact based).