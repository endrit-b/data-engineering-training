### Project Setup
***
> Follow the steps to get access and create your fork of the main repository.

**Setting up the repository**
* Open your browser and go to this link: https://gitlab.com
* Use your previously created account to login
* Follow this link to go to the main project in gitlab (maintained by PRIME): https://gitlab.com/endrit-b/data-engineering-training
* Click the fork button

> ![Fork repo](./_img/fork_instruction.png)
***

* Select your account name as the namespace/group to fork the project
* Add Endrit Bytyqi (@endrit-b) and Agon Lohaj (@agonlohaj) as Maintainers

> ![Fork repo](./_img/add_member.png)
***

* Make the project private.

> ![Fork repo](./_img/visibility.png)
***

Now in order to have the project locally, we will have to clone it using Source Tree. Follow the instructions listed here:
* Go to your forked project page
* Click the clone icon

> ![Fork repo](./_img/clone_instruction.png)
***

* Use the clone with HTTPS, copy the url
* Open Source tree on your local machine
* Go ahead and choose the File -> Clone/New option. You can also find it using the add button and going at the Clone Tab

> ![Fork repo](./_img/clone_new_source_tree.png)
***

* Paste the link at the source path, that you obtained from step 3
* Choose a folder at your local machine
* Click Clone


Now lets track the remote changes that we will publish. These are the new lectures that you will get at the next session
* From the "Repository" menu click "Add Remote"
* Name the remote to "Training"
* The URL/path will be the URL of the training repository located here: https://gitlab.com/endrit-b/data-engineering-training
* Name the remote to "Training"

> ![Fork repo](./_img/new_remote.png)
***

* When we do the next session, use the "Pull" functionality from Source Tree to get the next lecture into your "Forked" repository

> ![Fork repo](./_img/origin_master.png)
***


#### Java Configuration

During this training we'll be using Java 8 for developing data processing applications. So, let's setup also Java and Maven.

Custom installation:
Let's open the following link, which provides step-by-step instructions on the setup:
https://www.twilio.com/blog/2017/01/install-java-8-apache-maven-google-web-toolkit-windows-10.html

* Open IntelliJ then move click in File -> Open
    > ![Fork repo](./_img/intellij.png)