## Way Of Working

#### GitLab Setup

Before we explain the rules, make sure your GitLab Board (Under the Issues section) has the following Columns:
* Open: We will consider this our backlog
* Doing: The assignments that are under progress
* Review: The assignments that are under review by your trainer or assistant
* Done: The assignments that are reviewed and merged

> ![Fork repo](./_img/board_instruction.png)
***
> ![Fork repo](./_img/board_instruction_part_2.png)

Due to the [bug](https://gitlab.com/gitlab-org/gitlab-ce/issues/20704) on Gitlab, we are going to follow a workaround. That is to first remove the "Fork" Relationship from the project. Here are the steps required:

* Go to Repository Settings in General option
> ![Fork repo](./_img/general_settings.png)
***
* Go to Remove Fork Relationship section and perform the action
> ![Fork repo](./_img/remove_fork.png)
***
* Confirm the action by typing in the name of the repository "data-engineering-training"
> ![Fork repo](./_img/confirm_rmv_fork.png)
***

#### Rules
These are the following rules of the game that we will follow throughout the training:
1. Organise tasks in the GitLab board under the backlog section, as if he/she was working on a project.
2. For each assignments has to be written as a task in GitLab. Their acceptance criteria is the information of the assignment (it contains a description and a “Definition of Done”). Add it initially at the open issues column.
3. Every task will have its own feature branch that references the task.
4. When working on the assignment put the task in “doing” column and the Board - making changes in the feature branch (write multiple commits).
5. Perform a pull request when the feature is ready to be merged back. At this point, add the feature to “review” column.
6. This pull request has to be then approved by the trainer/assistant in order for it to be merged and considered done. 
    These are the steps on how this will proceed:
    7. You will make the code changes that fulfill the definition of done for the respective task for which the branch is created
    8. You will do a pull request (See below for more information)
    9. If everything is ok, your trainer or assistant will approve and merge it, at this point you can consider it done, thus move it to done (at the GitLab board)
    10. Otherwise the trainer or assistant has disapproved your changes and provided feedback to the reason why. At this point, go back to step 1 and restart.
    11. Pull requests let you tell others about changes you've pushed to a GitHub repository. Once a pull request is sent, interested parties can review the set of changes, discuss potential modifications, and even push follow-up commits if necessary.


##### Exercise 1

Step 1: Create your first task with the title "Exercise 1" and add it at doing:
> ![Fork repo](./_img/exercise_1.png)

Step 2: Click the title of the task to go to its details page
> ![Fork repo](./_img/exercise_2.png)

Step 3: Add the description: “In order to learn on how to exercise the way of working, I will change the “README.md” file to contain my name on it”

<!-- BREAK -->
Step 4: Create a branch for the task at GitLab, fields are all automatic, just hit the "Create Branch" button
> ![Fork repo](./_img/exercise_3.png)

Step 5: Go to Source Tree, and fetch all the remote branches

In case you face SSL verification issue please open terminal from SourceTree and run the command below:
   
```bash
git config --global http.sslVerify false

```

Step 6: Double-click to jump to the branch created at step 4
> ![Fork repo](./_img/exercise_4.png)

Step 7: Perform the task, using VS Code -> Add your name at the READE.md file

<!-- BREAK -->
Step 8: On Source Tree, commit and push the changes!
> ![Fork repo](./_img/exercise_5.png)

Step 9: Create the merge request, by:

1. Go at the merge request screen
> ![Fork repo](./_img/exercise_6.png)

2. Choose the branch created at step 4
> ![Fork repo](./_img/exercise_7.png)

Step 9: Move the task at the board to review!

Step 9: Wait for us to approve your merge request. Don't close it yourself. You are Done!

