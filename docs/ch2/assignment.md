### Home Assignment
***

1. Find if there is any error in the SQL statements below and fix them:

    ```sql
    use employees;
    
    SELECT first_name last_name FROM employees;
    
    SELECT emp_no, salary FROM salaries:
    
    SELECT first_name as 'First Name', last_name is 'Last Name' FROM employees;
    
    SELECT first_name, last_name, concat(first_name, ' '', last_name') as 'Name' FROM employees;
    
    SELECT salary, 
    salary * .01 as weekly,
    salary * .01 * 4 as monthly,
    salary * .01  52 as yearly  FROM salaries;
    
    SELECT from_date as original, DATE_FORMAT(from_date, "%M %d %YYYY") FROM salaries; 
    
    SELECT first_name, last_name, 
    concat(LEFT(first_name, 1), LEFT(last_name, 1))) as Initials from employees;
    
    SELECT * FROM employees WHERE first_name = 'Elvis' and last_name = 'Velasco'
    and first_name = 'Chenye' and last_name = 'Velasco';
    
    SELECT * FROM employees WHERE first_name IS IN ('Elvis', 'Sumant','Berni', 'Lillian' );
    
    select * from employees where first_name IS LIKE 'Elv%' and last_name IS LIKE '_e%';
    
    select * from employees where birth_date between ('1954-05-01' and '1956-04-20');
    
    select * from employees order by first_name DESCENDING;
    
    select * from employees order by emp_no LIMIT 0, 20, 88;
    ```
2. Generate a Report showing by year: total, max, min, and average salary
    * Exclude Managers from Report
    * Managers are defined in the table `DEPT_MANAGER`
    * Don’t worry about active dates at this time.
    * Hint use NOT IN clause


* To upload SQL script create a folder `solution` in root directory of the project
* Follow the [Way of Working](../ch1/way_of_working.md) while implementing the assignment.