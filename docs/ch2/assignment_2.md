### Home Assignment
***

1. Installation work:

    * [Install Ubuntu](https://tutorials.ubuntu.com/tutorial/tutorial-ubuntu-on-windows) as a subsystem in your Windows 10 machine
    * [Install Hadoop](https://medium.com/devilsadvocatediwakar/installing-hadoop-on-ubuntu-9493af1af12f) on Ubuntu
    * [Install Hive](https://medium.com/@maheshdeshmukh22/step-by-step-guide-on-how-to-install-hive-on-ubuntu-aeb9a72e950d) on Ubuntu
    * [Configure Hive & Mysql](https://www.guru99.com/installation-configuration-hive-mysql.html) on Ubuntu
    

2. Generate Orders Report:
    - Download `northwind` database dump from: https://gitlab.com/endrit-b/test_db
    - Import data using MySQL Workbench, by running sql script `northwind.sql` and `northwind-data.sql`
    - Show Customer First Name, Last Name
    - Shipping Company Name
    - Order Date (formatted as January, 1, 2018), Shipping Address (street, city, state, zip, country)
    - Product Code, Product Name, List Price, quantity ordered, and total cost of line item
    - Provide friendly column names
    - Format numbers to have commas and limit decimals to two places

3. Generate monthly profit report by item:
   - Report aggregated by year, by month, per item
   - Show total sales, cost, and profit List Price * order quantity - standard cost * order quantity 
   - Limit to order lines invoiced 
   - Show total revenue - List Price * order quantity
   - Limit to order lines invoiced

4. [BONUS] Generate Weekly Sales Report per Employee:
  - Report should list each employee and show zero if the employee had no sales
  - Should be values for each week company had sales
  - Use outer joins
  - Use ifnull function to provide zero values
  - Hint - you will need to use a subquery for order data


* To upload SQL script create a folder `solution` in root directory of the project
* Follow the [Way of Working](../ch1/way_of_working.md) while implementing the assignment.

