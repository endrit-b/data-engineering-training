### Introduction to SQL
***

#### So what is SQL?

**Structured Query Language (SQL)** is a special-purpose programming language designed for managing data held in a Relational Database Management System (RDBMS). 

Nowadays, SQL-like languages can also be used in Relational Data Stream Management Systems (RDSMS), or in "not-only SQL" (NoSQL) databases.

SQL consists of 3 major sub-languages:
1. Data Definition Language (DDL): to create and modify the structure of the database;
2. Data Manipulation Language (DML): to perform Read, Insert, Update and Delete operations on the data of the database;
3. Data Control Language (DCL): to control the access of the data stored in the database.


#### What is a Database Management System (DBMS)?

Database Management System (DBMS) is a set of tools which enables users 4 important operations:
- Data Definition - define the data being tracked
- Data Manipulation - add, update or remove data
- Data Retrieval - extract and report on the data in the database
- Administration - defining users on the system, security, monitoring, system administration


#### Entity-Relationship Model 

Proposes thinking of a database as a collection of entities, rather than being
used as a model on its own. You can look at ER Model as:

* As a tool to design relational databases.
* Building blocks: Entities and Attributes 

> <img src="./_img/er_d_1.png" width="400" height="250">    <img src="./_img/er_d_2.png" width="150" height="250">


##### Relational model characteristics
* A relation is a mathematical concept based on idea of sets, and is the mathematical term
for a table.
* A Relation is made up of 2 parts: Relational Schema and Relational Instance.
* A Relational Schema specifies: the name of a relation, and the attributes.
* A Relational Instance is a table made up of the attributes, or columns, and the tuples,
or rows.
* Degree refers to the number of attributes, or columns, in a relation.
* Cardinality refers to the number of tuples, or rows in a relation.
 
 > ![Fork repo](./_img/table.png)
 ***

##### Characteristics of a Database Table
- A database table is a lot like a spreadsheet in Excel.
- Data is kept in Columns and Rows.
- Each Column is assigned:
    - A Unique Name, identifying a human readable name of the column. (ie FIRST_NAME, LAST_NAME)
    - A Data Type (ie - String, Date, Time, Number, etc)
    - Optionally, constraints (ie - Is a value required?, Length of String, etc)
- Each Row is a distinct database Record.

#### Primary Key

A Primary Key is an optional special database column or columns used to identify a database record.
- Unique - There can be only one! Like Highlander!
- Could be username, user_id, or first_name and last_name.

- Surrogate Key is a type of Primary Key which used a unique generated value.
    - Should have no business value, and should never change.
    - Typically a system generated self incrementing number. Can be a unique string (UUID).
    - Considered a best practice in Relational Database Design.

#### Types of Relationships

Database Table Relations are defined through Foreign Key Constraints in conjunction with Primary Keys.

Building blocks of a relationship are:
* Entities
* Relationship sets, and 
* Crows foot notations (greater-than, less-than, vertical line)

    > <img src="./_img/rel_types.png" width="400" height="250"> 

##### 1. One-to-One Relationship

* For instance, a book written by one author
    > <img src="./_img/one2one.png" width="400" height="150"> 

##### 2. One-to-Many Relationship

* A book written by many authors
* Crows foot notation, less-than
* Could be also called many-to-one relationship for many authors that write a single book

    > <img src="./_img/one2many.png" width="400" height="150"> 

##### 3. Many-to-Many Relationship 
* Crows foot notation, less-than
* Many books written by many authors
* Many authors writing by many books

    > <img src="./_img/many2many.png" width="400" height="150"> 


***

#### Popular DBMSs
Based on [DB-Engines Ranking](https://db-engines.com/en/ranking_trend), these are top relational database management systems according to their popularity:

* Oracle
* MySQL
* MSSQL Server
* PostgreSQL
* DB2
* MariaDB


  > ![Fork repo](./_img/db_engines.png)
  
