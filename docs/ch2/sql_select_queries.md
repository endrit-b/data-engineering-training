
### The SQL Select Statement
***
> This section will walk through SQL Select queries used to retrieve data from the database.


**The SELECT statement** is used to retrieve data from a database.

```sql
-- select all columns
SELECT * FROM employees;  -- (*) not always recommended to use

-- select first_name and last_name from employees table
SELECT first_name, last_name FROM employees;

-- select emp_no and salary from salaries table
SELECT emp_no, salary FROM salaries;
```

**SELECT with Alias (AS)**
* When extracting reports, proper column naming is required, thus column `aliases` are used. 
SQL aliases are used to temporarily rename a table or a column. They are generally used to improve readability.

```sql
-- Alias example 1
SELECT first_name as FirstName, last_name as LastName FROM employees;

-- Alias example 2  
SELECT first_name as 'First Name', last_name as 'Last Name' FROM employees;
```

**Select with DISTINCT**

The SELECT DISTINCT statement is used to return only distinct values from a table.

```sql
SELECT DISTINCT last_name FROM employees;
SELECT COUNT(DISTINCT (first_name)) FROM employees;
``` 

**Select with WHERE Clause**

> The WHERE clause is used to tell MySQL the condition we want to use when filtering for data.

* Equal and NOT Equal operators

```sql
-- The one with equal operator
SELECT * FROM employees WHERE first_name = 'Elvis';

-- The one with not equal operator
SELECT * FROM employees WHERE first_name <> 'Elvis';
-- or
SELECT * FROM employees WHERE first_name != 'Elvis';

-- Counting with Where clause
SELECT count(*) FROM employees WHERE first_name = 'Elvis';
```

* SQL Where clause with AND, OR and NOT
> The WHERE clause uses AND, OR, and NOT operators to combine conditions.

```sql
SELECT * FROM employees WHERE first_name = 'Elvis' and gender = 'M';

SELECT count(*) FROM employees WHERE first_name = 'Elvis' and gender = 'M';

SELECT * FROM employees WHERE first_name = 'Elvis' or first_name = 'Chenye' and NOT (last_name = 'Velasco' and last_name='Luca');
```

* SQL Where clause with IN and NOT IN operators
> The IN operator allows you to specify multiple values in a WHERE clause.

```sql
SELECT * FROM employees WHERE first_name IN ('Elvis', 'Sumant','Berni', 'Lillian' );

SELECT count(*) FROM employees WHERE first_name IN ('Elvis', 'Sumant','Berni', 'Lillian' );

SELECT count(*) FROM employees WHERE first_name IN ('Elvis', 'Sumant','Berni', 'Lillian' ) 
AND last_name NOT IN ('Redmiles', 'Feldhoffer', 'Androutsos', 'Schaar');
```

* Dealing with Empty (NULL) values in Where caluse
> NULL represents an empty value in a caloumn of a table. NULL is not equal to "" (blank string) or 0 (in case of integer).
   
```sql
SELECT * FROM titles WHERE to_date IS NULL;

SELECT * FROM titles WHERE to_date IS NOT NULL;
```

* SQL Where clause with Greater-than and Less-than operators

```sql
SELECT * FROM salaries WHERE salary > 66961;

SELECT count(*) FROM salaries WHERE salary > 66961;

SELECT count(*) FROM salaries WHERE salary < 66961;

SELECT count(*) FROM salaries WHERE salary > 66961 AND from_date > '1989-06-25';
```

* SQL Where with Range Queries - BETWEEN

```sql
select count(*) from salaries where salary >= 66074 and salary <= 71046;

select count(*) from salaries where salary between 66074 and 71046;

select count(*) from employees where birth_date between '1954-05-01' and '1956-04-20';

select count(*) from employees where birth_date not between '1954-05-01' and '1956-04-20';
```

* SQL Where clause with LIKE and NOT LIKE
    > (_) in a LIKE clause is a wildcard that matches a single character.

    > (%) in a LIKE clause is a wildcard that matches any character.

```sql
select * from employees where first_name like 'E%';

select * from employees where first_name like 'Elv%' and last_name like '_e%';

select * from employees where first_name like 'Elv%' and last_name like '_e%' 
and last_name not like '%n' ;
```

**The SQL SELECT with CASE or IF**
> CASE and IF operators are used to project a result based on the condition they match

```sql
-- The one with CASE
select salary, CASE WHEN salary >= 45000 THEN 'Satisfied' ELSE 'Not Satisfied' END AS 'Remark' from salaries;

-- The one with IF
select salary, IF(salary >= 45000, 'Satisfied', 'Not Satisfied') AS 'Remark' from salaries;
-- Which means: IF salary >= 45000 is TRUE then return 'Satisfied' ELSE return 'Not Satisfied'
```

**SELECT with a ORDER BY clause**

* ORDER BY X - X can be any datatype:
    * NULLs precede non-NULLs.
    * The default is ASC (lowest to highest)
    * Strings (VARCHAR, etc)
    * ENUMs are ordered by the declaration order of its strings.

```sql
-- Use cases:
ORDER BY x ASC -- same as default
ORDER BY x DESC -- highest to lowest
ORDER BY lastname, firstname -- typical name sorting; using two columns
ORDER BY submit_date DESC -- latest first
ORDER BY submit_date DESC, id ASC -- latest first, but fully specifying order.
```

```sql
select * from salaries order by from_date, salary desc;

select * from employees order by first_name;

select * from employees where first_name = 'Elvis' order by gender desc, birth_date desc, last_name;
```

**SELECT with a LIMIT clause**
   > LIMIT clause limits the number of records we want to retrieve from the database

```sql
SELECT 
    first_name, last_name
FROM
    employees
ORDER BY first_name
LIMIT 10;
```
   * __Best Practice__: Always use ORDER BY when using LIMIT; otherwise the rows you will get will be unpredictable.

```sql
SELECT 
    first_name, last_name
FROM
    employees
ORDER BY first_name
LIMIT 2,1;

-- Alternatively:
SELECT 
    first_name, last_name
FROM
    employees
ORDER BY first_name, last_name 
LIMIT 1 OFFSET 2;
```

When a LIMIT clause contains two numbers, it is interpreted as LIMIT offset,count. 
So, in this example the query skips two records and returns one.


**The SQL Sub-queries**

* SQL Sub-queries are SELECT queries (aka nested query) declared within a valid SQL clause or used in conjunction with an SQL operator within WHERE clause.

```sql
SELECT 
    *
FROM
    employees
WHERE
    first_name IN (SELECT DISTINCT
            first_name
        FROM
            employees
        WHERE
            first_name LIKE 'E%');
```

* Concatenating column values with CONCAT() function.

```sql
SELECT first_name, last_name, CONCAT(first_name, last_name) as 'Name' FROM employees;
-- concat by an space char
SELECT first_name, last_name, CONCAT(first_name, ' ', last_name) as 'Name' FROM employees;

-- construct initials from first and last name
SELECT first_name, last_name, CONCAT(LEFT(first_name, 1), LEFT(last_name, 1)) as 'Initials' FROM employees;
-- Similarly, RIGHT() function can be used
```


**SQL Arithmetic Operators**
***
   > ![](./_img/arithmetic_ops.png)

> Arithmetic operators can be applied to any numeric column.

```sql
-- Let's lower the salary to a more realistic value
SELECT salary, salary * .01 as weekly FROM salaries;

-- The one with weekly, monthly and yearly
SELECT salary, 
salary * .01 as weekly,
salary * .01 * 4 as monthly,
salary * .01 * 52 as yearly  FROM salaries;

-- An example with divide and modulo operators
SELECT salary,
(salary + 200) * .01 as add_first,
salary *.01 / 7 as daily,
salary DIV 3 as div_op,
salary % 3 as mod_op FROM salaries;
```

