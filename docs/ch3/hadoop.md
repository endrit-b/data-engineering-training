### Introduction to Hadoop
***

The Apache Hadoop software library is a framework that allows for the distributed processing of large data sets across clusters of computers using simple programming models. It is designed to scale up from single servers to thousands of machines, each offering local computation and storage. Rather than rely on hardware to deliver high-availability, the library itself is designed to detect and handle failures at the application layer, so delivering a highly-available service on top of a cluster of computers, each of which may be prone to failures.

Hadoop is designed to be fault tolerant. You tell it how many times to replication data. Then when a datanode crashes data is not lost.

Hadoop uses a master-slave architecture. The basic premise of its design is to Bring the computing to the data instead of the data to the computing. That makes sense. It stores data files that are too large to fit on one server across multiple servers. Then when is does Map and Reduce operations it further divides those and lets each server in the node do the computing. So each node is a computer and not just a disk drive with no computing ability.

**Architecture**
<img src="./img/hadoop_architecture.png">

Here are the main components of Hadoop.

* **Namenode** — controls operation of the data jobs.
* **Datanode** — this writes data in blocks to local storage. And it replicates data blocks to other datanodes. DataNodes are also rack-aware. You would not want to replicate all your data to the same rack of servers as an outage there would cause you to loose all your data.
* **SecondaryNameNode** — this one take over if the primary Namenode goes offline.
* **JobTracker** — sends MapReduce jobs to nodes in the cluster.
* **TaskTracker** — accepts tasks from the Job Tracker.
* **Yarn** — runs the Yarn components ResourceManager and NodeManager. This is a resource manager that can also run as a stand-alone component to provide other applications the ability to run in a distributed architecture. For example you can use Apache Spark with Yarn. You could also write your own program to use Yarn. But that is complicated.
* **Client Application** — this is whatever program you have written or some other client like Apache Hive, Apache Spark, Apache Pig etc
* **Application Master** — runs shell commands in a container as directed by Yarn.

**Hadoop storage mechanisms**

Hadoop can store its data in multiple file formats, mainly so that it can work with different cloud vendors and products. Here are some:

```
Hadoop—these have the hdfs:// file prefix in their name.
For example hdfs://masterServer:9000/folder/file.txt is a valid file name.

S3—Amazon S3.  The file prefix is s3n://

file—file:// causes Hadoop to use any other distributed file system.
```
Hadoop also supports Windows Azure Storage Blobs (WASB), MapR, FTP, and others.

**Writing to data files**

Hadoop is not a database. That means there is no random access to data and you cannot insert rows into tables or lines in the middle of files. Instead Hadoop can only write and delete files, although you can truncate and append to them, but that is not commonly done.

**Hadoop as a platform for other products**

We are going to use Hadoop as a platform for products like Hive and Spark!