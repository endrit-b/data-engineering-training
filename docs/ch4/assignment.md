#### Home assignment
***

1. Create a Spark program to read the airport data from src/main/resources/airports.txt, find all the airports which are located in United States and output the airport's name and the city's name to solution/airports_in_usa.txt.
    
       Each row of the input file contains the following columns:
       Airport ID, Name of airport, Main city served by airport, Country where airport is located, IATA/FAA code,
       ICAO Code, Latitude, Longitude, Altitude, Timezone, DST, Timezone in Olson format

       Sample output:
       "Putnam County Airport", "Greencastle"
       "Dowagiac Municipal Airport", "Dowagiac"

2.  Create a Spark program to read the airport data from src/main/resources/airports.text,  find all the airports whose latitude are bigger than 40.
  Then output the airport's name and the airport's latitude to solution/airports_by_latitude.txt.
    ```java
    Each row of the input file contains the following columns:
    Airport ID, Name of airport, Main city served by airport, Country where airport is located, IATA/FAA code,
    ICAO Code, Latitude, Longitude, Altitude, Timezone, DST, Timezone in Olson format
    
    Sample output:
    "St Anthony", 51.391944
    "Tofino", 49.082222
    ```
      

3. Create a Spark program to generate a new RDD which contains the hosts which are accessed on BOTH days.
   Save the resulting RDD to "solution/nasa_logs_same_hosts.csv" file.
       
   ```java
    "src/main/resources/nasa_19950701.tsv" file contains 10000 log lines from one of NASA's apache server for July 1st, 1995.
    "src/main/resources/nasa_19950801.tsv" file contains 10000 log lines for August 1st, 1995
    
    Example output:
    vagrant.vf.mmc.com
    www-a1.proxy.aol.com
    .....
    
    Keep in mind, that the original log files contains the following header lines.
    host	logname	time	method	url	response	bytes
    
    Make sure the head lines are removed in the resulting RDD.
   ```
       
