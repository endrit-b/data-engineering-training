### Spark RDD
***

The main abstraction Spark provides is a resilient distributed dataset (RDD), which is a collection of elements partitioned across the nodes of the cluster that can be operated on in parallel.


A second abstraction in Spark is shared variables that can be used in parallel operations. By default, when Spark runs a function in parallel as a set of tasks on different nodes, it ships a copy of each variable used in the function to each task. 

Sometimes, a variable needs to be shared across tasks, or between tasks and the driver program. 

Spark supports two types of shared variables: 
* Broadcast variables - which can be used to cache a value in memory on all nodes, and 
* Accumulators, which are variables that are only “added” to, such as counters and sums.

### Apache Spark with Java
Throughout this course on Apache Spark, we'll be using Java to develop data processing applications.

The first thing a Spark program must do is to create a JavaSparkContext object, which tells Spark how to access a cluster. To create a SparkContext you first need to build a SparkConf object that contains information about your application.


```java
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.SparkConf;

SparkConf conf = new SparkConf().setAppName(appName).setMaster(master);
JavaSparkContext sc = new JavaSparkContext(conf);
```

> The appName parameter is a name for your application to show on the cluster UI. master is a Spark, Mesos or YARN cluster URL, or a special “local” string to run in local mode

#### Resilient Distributed Datasets (RDDs)

**There are two ways to create RDDs**

1. Parallelized collection in your driver program, 
    ```java 
    List<Integer> data = Arrays.asList(1, 2, 3, 4, 5);
    JavaRDD<Integer> distData = sc.parallelize(data);
    ```
2. External storage system, such as a shared filesystem, HDFS, or Amazon S3.
    ```java 
    JavaRDD<String> distFile = sc.textFile("data.txt");
    ```
There are other data sources which can be integrated with Spark and used to create RDDs including JDBC, Cassandra, and Elastisearch, etc.

**Characteristics of RDDs**
* RDDs are Distributed
    * Each RDD is broken into multiple pieces called partitions, and these partitions are divided across the clusters.
* RDDs are Immutable
    * They cannot be changed after they are created.
    * Immutability makes it safe to share RDDs across processes.
* RDDs are Resilient
    * RDDs are a deterministic function of their input. This plus immutability also means the RDDs parts can be recreated at any time.
    * In case of any node in the cluster goes down, Spark can recover the parts of the RDDs from the input and pick up from where it left off.


#### RDD Operations


* RDDs support two types of operations:
    * **Transformations** - which create a new dataset from an existing one, and 
    * **Actions** - which return a value to the driver program after running a computation on the dataset.

All **transformations in Spark are lazy**, in that they do not compute their results right away. Instead, they just remember the transformations applied to some base dataset (e.g. a file). The transformations are only computed when an action requires a result to be returned to the driver program.

##### Transformations
The two most common transformations are `filter()` and `map()`.

* **`filter(func)`**

    Takes in a function and returns an RDD formed by selecting those elements which pass the filter function.
     ```java
     JavaRDD<String> airportsInUSA = airports.filter(line -> line.split(",")[3].equals("\"United States\""));
     ```
 
* **`map(func)`**

    Takes in a function and passes each element in the input RDD through the function, with the result of the function being the new value of each element in the resulting RDD.
 
    ```java
    JavaRDD<String> lowerCaseLines = lines.map(line -> line.toUpperCase());
    ```
    
* **`flatMap(func)`**
    
    Similar to map, but each input item can be mapped to 0 or more output items (so func should return a Seq rather than a single item).
    ```java
    JavaRDD<String> words = lines.flatMap(s -> Arrays.asList(s.split(" ")).iterator());
    ```

**Exercise 1:** Let's write a Spark app that reads file `airports.txt` and returns all the airport names.
***

**Set operations which are performed on one RDD** 
- `sample` - The sample operation will create a random sample from an RDD
- `distinct` - The distinct transformation returns the distinct rows from the input RDD.

**Set operations which are performed on two RDDs**

- `union` - Union operation gives us back an RDD consisting of the data from both input RDDs.
    ```java
    rdd1 = rdd1.union(rdd2);
    ```
- `intersection` - Intersection operation returns the common elements which appear in both input RDDs.
    ```java
    rdd1 = rdd1.intersection(rdd2);
    ```
- `subtract` - Subtract operation takes in another RDD as an argument and returns us an RDD that only contains element present in the first RDD and not the second RDD.
    ```java
    rdd1 = rdd1.subtract(rdd2);
    ```
- `cartesian product` - Cartesian transformation returns all possible pairs of a and b where a is in the source RDD and b is in the other RDD.
    ```java
    rdd1 = rdd1.cartesian(rdd2);
    ```

**Exercise 2:** Create two RDDs and apply `union`, `intersection`, and `subtract` operations.

***

##### RDD Actions

Actions are the operations which will return a final value to the driver program or persist data to an external storage system.

* **`collect()`**
    Return all the elements of the dataset as an array at the driver program.
    ```java
    List<String> words = wordRdd.collect();
    ```
* **`count()`**
    Return the number of elements in the dataset.
    ```java
    wordRdd.count();
    ```

* **`countByValue()`**
    Will look at unique values in the each row of your RDD and return a map of each unique value to its count.
    ```java
    Map<String, Long> wordCounts = words.countByValue();
    ```
    
* **`take()`**
    take action takes n elements from an RDD.
    ```java
    List<String> words = wordRdd.take(3);
    ```

* **`saveAsTextFile()`**
    Can be used to write data out to a distributed storage system such as HDFS or Amazon S3, or even local file system.
    ```java
    airportsRdd.saveAsTextFile("out/transformed_airport_data.txt");
    ```

* **`reduce(func)`**
    Aggregate the elements of the dataset using a function func (which takes two arguments and returns one).
    ```java
    Integer product = integerRdd.reduce((x, y) -> x * y);
    ```

**Exercise 2:**
Create a Spark program to read the first 100 prime numbers from src/main/resources/prime_nums.text, print the sum of those numbers to console.
Each row of the input file contains 10 prime numbers separated by spaces.
***


