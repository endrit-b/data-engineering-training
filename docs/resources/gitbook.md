### Serve content as a Gitbook
***

#### Prerequisites

Installing GitBook is easy and straightforward. Your system just needs to meet these two requirements:
  
  * [NodeJS](https://nodejs.org/en/download/) (v4.0.0 and above is recommended)
  * Windows, Linux, Unix, or Mac OS X

#### Installation
The best way to install GitBook is via [NPM](https://www.npmjs.com/). At the terminal prompt, simply run the following command to install GitBook:
```bash
$ npm install gitbook-cli -g

```

#### Run Gitbook
```bash
$ cd <project_root_dir>
$ gitbook serve

```