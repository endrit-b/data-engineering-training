package io.goprime.training.SparkRDD;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.Arrays;

//import org.apache.log4j.Level;
//import org.apache.log4j.Logger;


public class SumPrimeNumsProblem {

    public static void main(String[] args) {

        // If you want to disable logging uncomment line below, and importing lines at the top
        // Logger.getLogger("org").setLevel(Level.OFF);

        // Set Spark configs
        SparkConf conf = new SparkConf()
                .setAppName("primeNumbers")
                .setMaster("local[*]");

        // Instantiate SparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        // Create an RDD by reading prime_nums.txt file
        JavaRDD<String> primeNums = sc.textFile("src/main/resources/prime_nums.txt");

        // Use flatMap(...) transformation to unbox each array of lines into one bag of numbers
        JavaRDD<String> numbers = primeNums.flatMap(line -> Arrays.asList(line.split("\\s+")).iterator());

        // remove any empty element from the rdd
        JavaRDD<String> validNumbers = numbers.filter(number -> !number.isEmpty());

        // Parse elements to integers
        JavaRDD<Integer> intNumbers = validNumbers.map(number -> Integer.valueOf(number));

        // Calculate sum
        int sum = intNumbers.reduce((x, y) -> x + y);

        System.out.println("Total sum of the numbers in the file is: " + sum);
    }
}
