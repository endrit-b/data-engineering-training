package io.goprime.training.SparkSQL;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.io.File;

public class HiveExample {

    public static void main(String[] args) {
        String warehouseLocation = new File("spark-warehouse").getAbsolutePath();
        SparkSession spark = SparkSession
                .builder()
                .appName("Java Spark Hive Example")
                .master("local")
                .config("hive.metastore.uri", "thrift://localhost:9083")
                .enableHiveSupport()
                .getOrCreate();

        // Create database
        spark.sql("CREATE DATABASE IF NOT EXISTS prime_tut");
        spark.sql("USE prime_tut");

        // Create table and load the data from file
        spark.sql("CREATE TABLE IF NOT EXISTS src (key INT, value STRING) USING hive");
        spark.sql("LOAD DATA LOCAL INPATH 'src/main/resources/key_value.txt' INTO TABLE src");

        // Queries are expressed in HiveQL
        spark.sql("SELECT * FROM src").show();

        // Create DS from CSV
        Dataset<Row> students = spark.read().option("header", "true").csv("src/main/resources/students.csv");

        students.write().mode("overwrite").saveAsTable("students");

        spark.stop();
    }
}
